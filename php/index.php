<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type");
header('Content-Type: application/x-www-form-urlencoded');

require('./mailchimp/Mailchimp.php');

$apiKey = "5e1c5b07cc2d53c2556c1b6061e1c964-us17";
$listID = "04e2133eb3";

$Mailchimp = new Mailchimp($apiKey);
$Mailchimp_Lists = new Mailchimp_Lists($Mailchimp);

try
{
  $subscriber = $Mailchimp_Lists->subscribe(
    $listID,
    ['email' => $_POST['email_address']],
    ['FNAME' => '', 'LNAME' => ''],
    'text',
    FALSE,
    FALSE
  );
}
catch (Exception $e)
{
  $arr = [
    'message' => $e->getMessage(),
    'status_code' => $e->getCode()
  ];
  echo json_encode($arr);
}

if ( ! empty($subscriber['leid']) )
{
  $arr = [
    'message' => 'Thank you!',
    'status_code' => 200,
    'custom_message' => 'Subscription is confirmed! You will get all the awesome updates!'
  ];
  echo json_encode($arr);
}