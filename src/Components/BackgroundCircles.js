import React, { Component } from 'react'
import InnerCircle from '../Graphics/InnerCircle.svg'
import OuterCircle from '../Graphics/OuterCircle.svg'

import './BackgroundCircles.css'

class BackgroundCircles extends Component {
  render(){
    return(
      <div id="bg-circles">
        <img alt="" src={OuterCircle} id="outer-circle" className="background-circle" />
        <img alt="" src={InnerCircle} id ="inner-circle" className="background-circle" />
      </div>
    )
  }
}

export default BackgroundCircles