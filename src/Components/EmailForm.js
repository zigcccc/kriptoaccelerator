import React, { Component } from 'react'
import axios from 'axios'
import qs from 'qs'
import './EmailForm.css'

import SendButton from './SendButton'
import ErrorMessage from './ErrorMessage'

class EmailForm extends Component {
  constructor(props){
    super(props)
    this.state = {
      formProps: this.props.formProps,
      base_api: this.props.mailchimpConfig.base_api,
      inputText: '',
      loading: false,
      dataSend: false,
      error: false,
      errorMessage: '',
      successMessage: ''
    }
  }

  handleInputChange(e){
    let inputValue = e.target.value
    this.setState({
      ...this.state,
      inputText: inputValue
    })
  }

  componentWillMount(){}

  submitForm(e){
    e.preventDefault()

    this.setState({
      ...this.state,
      loading: true
    })

    let reqBody = {
      email_address: this.state.inputText,
      status: 'subscribed'
    }

    axios.post(this.state.base_api, qs.stringify(reqBody))
      .then((res) => {
        let serverResponse = res.data
        if(serverResponse.status_code !== 200){
          this.setState({
            ...this.state,
            loading: false,
            dataSend: false,
            successMessage: '',
            error: true,
            errorMessage: serverResponse.message
          })
        }
        else {
          this.setState({
            ...this.state,
            error: false,
            dataSend: true,
            loading: false,
            errorMessage: '',
            successMessage: serverResponse.message
          })
        }
      })
      .catch((err) => {
        console.log(err)
      })
  }

  render(){
    return(
      <div id="form-container">
        <ErrorMessage hasError={this.state.error} errorMessage={this.state.errorMessage} />
        <form className="main-form">
            <input 
              onChange={this.handleInputChange.bind(this)}
              className="email-input"
              name="email-address"
              type="email"
              placeholder={this.state.formProps.inputPlaceholder} 
              required
            />
            <SendButton successMessage={this.state.successMessage} isLoading={this.state.loading} dataSend={this.state.dataSend} handleSubmit={this.submitForm.bind(this)} />
        </form>
      </div>
    )
  }
}

export default EmailForm