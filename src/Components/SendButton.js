import React, { Component } from 'react'
import './SendButton.css'

class SendButton extends Component {
  render(){
    if(!this.props.isLoading && !this.props.dataSend){
      return(
        <input 
          className="submit-btn"
          name="submit"
          onClick={this.props.handleSubmit}
          type="submit"
          value="Send"
        />
      )
    }
    else if (this.props.isLoading && !this.props.dataSend) {
      return(
        <input 
          className="submit-btn submit-btn-loading"
          name="submit"
          disabled
          type="submit"
          value="Loading..."
        />
      )
    }
    else {
      return (
        <input 
          className="submit-btn submit-btn-data-send"
          name="submit"
          disabled
          type="submit"
          value={this.props.successMessage}
        />
      )
    }
  }
}

export default SendButton