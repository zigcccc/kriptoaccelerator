import React, { Component } from 'react'
import './ErrorMessage.css'

class ErrorMessage extends Component {
  render(){
    if(this.props.hasError){
      return(
        <div className="error-message">
          <p>{this.props.errorMessage}</p>
        </div>
      )
    }
    else{
      return null
    }
  }
}

export default ErrorMessage