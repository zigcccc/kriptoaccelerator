import React, { Component } from 'react'
import logo from './LogoLight.svg'
import './App.css'

import BackgroundCircles from './Components/BackgroundCircles'
import EmailForm from './Components/EmailForm'

import Content from './content.json'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      content: {}
    }
  }

  componentWillMount(){
    this.setState({
        ...this.state,
        content: Content
      }
    )
  }

  render() {
    let siteContent = this.state.content
    return (
      <div className="App">
        <BackgroundCircles />
        <div id="content">
          <h1 className="App-title">
            <img src={logo} className="App-logo" alt="Kripto Accelerator" />
          </h1>
          <p className="App-intro">{siteContent.introText}</p>
          <EmailForm mailchimpConfig={siteContent.mailchimp} formProps={siteContent.emailInput} />
        </div>
      </div>
    );
  }
}

export default App
